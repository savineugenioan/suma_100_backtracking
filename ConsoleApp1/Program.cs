﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static int Generare(ref int[] v)
        {
            bool x = false;
            int i;
            for (i = 7; i >= 0; i--)
                if (v[i] != 2)
                {
                    x = true;
                    break;
                }

            if (x == true)
            {
                v[i]++;
                for (int j = i + 1; j < 8; j++)
                    v[j] = 0;
                return 1;
            }
            else
            {
                return 0;
            }
        }
        static int Concat(int aux, int i)
        {
            int x=aux;
            x = x * 10 + i + 2;
            return x;
        }
        static int Calc_Suma(int[] v)
        {
            int n,i,s=0,k=0;
            n = 9;
            for (i = 0; i < 8; i++)
                if (v[i] == 0)
                    n = n - 1;
            int[] w = new int[n];
            int aux=0;
            for(i=0;i<v.Length;i++)
            {
                if (v[i] == 0)
                {
                    if (aux == 0)
                        aux = i + 1;
                    aux=Concat(aux, i);
                }
                else if(v[i] != 0)
                {
                    if (aux != 0)
                    {
                        w[k] = aux;
                        aux = 0;
                        k++;
                    }
                    else
                    {
                        w[k] = i + 1;
                        k++;
                    }
                }     
            }
            if (aux != 0)
                w[k] = aux;
            else
                w[k] = 9;

            //for (i = 0; i < w.Length; i++)
            //    Console.Write("{0} ", w[i]);
            //Console.WriteLine();
            k = 1;
            s = s + w[0];
            for (i = 0; i < v.Length; i++)
            {
                if (v[i] == 1)
                {
                    s = s + w[k]; k++;
                }
                else if (v[i] == 2)
                {
                    s = s - w[k]; k++;
                }
            }
            return s;
        }
        static void Main(string[] args)
        {
            int[] v = new int[8];
            int i, s = 0;
            for (i = 0; i < 8; i++)
                v[i] = 0;
            while (Generare(ref v) == 1)
            {
                s=Calc_Suma(v);

                if (s == 100)
                {
                    int k = 0;
                    for (i = 1; i < 9; i++)
                    {
                        Console.Write(i);
                        switch (v[k])
                        {
                            case 0: break;
                            case 1: Console.Write("+"); break;
                            case 2: Console.Write("-"); break;
                        }
                        k++;
                    }
                    Console.WriteLine('9');
                }

                //for (i = 0; i < 8; i++)
                //    Console.Write(v[i]);
                //Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}